var ImageminPlugin = require("imagemin-webpack-plugin").default;
const MomentLocalesPlugin = require("moment-locales-webpack-plugin");

module.exports = {
  productionSourceMap: false,
  pluginOptions: {
    webpackBundleAnalyzer: {
      openAnalyzer: false
    }
  },
  css: {
    extract: process.env.NODE_ENV === "production",
    loaderOptions: {
      sass: {
        data: `@import "@/assets/scss/element-variables.scss";`
      }
    },
    sourceMap: true
  },
  configureWebpack: {
    devtool: "source-map",
    plugins: [
      new ImageminPlugin({
        disable: process.env.NODE_ENV !== "production",
        pngquant: { quality: "80-90" }
      }),
      new MomentLocalesPlugin({
        localesToKeep: ["es-mx"]
      })
    ]
  },
  chainWebpack: config => {
    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule.use("vue-svg-loader").loader("vue-svg-loader");

    const vueRule = config.module.rule("vue");
    vueRule.use("vue-svg-inline-loader").loader("vue-svg-inline-loader");

    const fontRule = config.module.rule("fonts");
    fontRule.uses.clear();
    fontRule.use("url-loader").loader("url-loader");
  }
};
