import Vue from "vue";

const VueScrollTo = require("vue-scrollto");

Vue.use(VueScrollTo, {
  offset: -100
});
