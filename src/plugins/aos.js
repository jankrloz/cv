import AOS from "aos";
import "aos/dist/aos.css"; // You can also use <link> for styles

const aos = {
  AOS,
  config: {
    disable: "mobile",
    useClassNames: false,
    duration: 500,
    once: true
  }
};
export default aos;
