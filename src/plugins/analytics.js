import Vue from "vue";
import VueAnalytics from "vue-analytics";

Vue.use(VueAnalytics, {
  id: "UA-138258228-1",
  autoTracking: {
    screenview: true
  },
  debug: {
    enabled: process.env.NODE_ENV !== "production",
    sendHitTask: process.env.NODE_ENV === "production"
  }
});
