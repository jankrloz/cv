import Vue from "vue";
import { Row, Col, Tooltip, Popover, Progress, Button, Card } from "element-ui";
import lang from "element-ui/lib/locale/lang/en";
import locale from "element-ui/lib/locale";

locale.use(lang);

Vue.use(Row);
Vue.use(Col);
Vue.use(Tooltip);
Vue.use(Popover);
Vue.use(Progress);
Vue.use(Button);
Vue.use(Card);
