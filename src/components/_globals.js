import Vue from "vue";

const globalRequiredComponents = require.context("./globals", false, /\.vue$/);

globalRequiredComponents.keys().forEach(filename => {
  const componentConfig = globalRequiredComponents(filename);

  const componentName = filename.replace(/.\//, "").replace(/\.\w+$/, "");

  Vue.component(componentName, componentConfig.default || componentConfig);
});
