import Vue from "vue";

// Root component
import App from "./App.vue";

// Plugins
import "./plugins/element.js";
import "./plugins/analytics";
import "./plugins/vue-particles.js";
import "./plugins/vue-scrollto.js";

// Globals
import "./assets/scss/global.scss";
import "./components/_globals";

Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
